# ansible-role-tmate

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
compiling and deploying the [tmate-ssh-server](https://tmate.io/)
software.
